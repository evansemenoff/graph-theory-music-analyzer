import numpy as np
import networkx as nx
import matplotlib.pyplot as mpl
import math

def readmelody(fname):
    f = open(fname, "r")

    data = []
    for line in f:
        data.append(line.rstrip().rsplit(","))

    notes = []

    for i in range(len(data)):
        if i % 2 == 0:
            print(data[i])
            notes.append(int(data[i][4]))

    return notes

def readharmony(fname, harmony_size):
    f = open(fname, "r")
    data = []
    for line in f:
        data.append(line.rstrip().rsplit(" "))

    datastriped = []
    for i in range(len(data)):
        if i % 2 == 0:
            datastriped.append(data[i])

    data = datastriped

    temporallookup = {}
    temporalindex = 0

    for note in data:
        if int(note[0]) not in temporallookup:
            temporallookup[int(note[0])] = temporalindex
            temporalindex += 1


    notesandtime = {}

    maxnote = 0
    for note in data:
        notetiming = temporallookup[int(note[0])]
        thisnote = int(note[3].rsplit("=")[1])
        if notetiming in notesandtime:
            notesandtime[notetiming] += [thisnote]
        else:
            notesandtime[notetiming] = [thisnote]
        if thisnote > maxnote:
            maxnote = thisnote
    notesandtime["MAX"] = maxnote

    harmonic_dict = {}

    if harmony_size > 1:
        for key in notesandtime:
            if key != "MAX" and len(notesandtime[key]) > harmony_size:
                harmonic_dict[key] = notesandtime[key]

        harmonic_dict["MAX"] = maxnote
        return harmonic_dict


    return notesandtime



def readbook(book):
    f = open(book, "r")
    data = []
    for line in f:
        data.append(line.rstrip().rsplit(" "))

    wordsinord = []

    for line in data:
        wordsinord += line

    mydict = {}
    ind = 0
    for i in wordsinord:
        if i not in mydict:
            mydict[i] = ind
            ind += 1
    final_mot = [mydict[i] for i in wordsinord]
    return final_mot


def product(x):
    it = 1
    for i in x:
        it *= i
    return it


def windowedmusic(fname, winsize, bigslicer, analysistype):

    piece = analysistype(fname)
    motifs = []
    if bigslicer:

        for i in range(0, len(piece) - winsize, winsize):
            myslice = piece[i:i+winsize]
            motifs.append(product(myslice))
    else:
        for i in range(len(piece) - winsize):
            myslice = piece[i:i+winsize]
            motifs.append(product(myslice))

    mydict = {}
    ind = 0
    for i in motifs:
        if i not in mydict:
            mydict[i] = ind
            ind += 1

    final_mot = [mydict[i] for i in motifs]
    return final_mot


def gen_dirweighted_adjmat(music):

    matdims = max(set(music)) + 1
    myadjmat = np.zeros((matdims,matdims), dtype=int)
    for i in range(len(music) - 1):
        mynote = music[i]
        nextnot = music[i + 1]
        myadjmat[mynote][nextnot] += 1
    return myadjmat


def gen_weighted_adjmat(music):

    if isinstance(music, dict):
        matdims = music["MAX"] + 1
        myadjmat = np.zeros((matdims, matdims), dtype=int)
        del music["MAX"]
        for key in music.keys():
            print(key)
            notelist = music[key]
            for i in range(len(notelist)):
                for j in range(i):
                    myadjmat[notelist[i]][notelist[j]] += 1
                    myadjmat[notelist[j]][notelist[i]] += 1


    else:
        matdims = max(set(music)) + 1
        myadjmat = np.zeros((matdims, matdims), dtype=int)
        for i in range(len(music) - 1):
            mynote = music[i]
            nextnot = music[i + 1]
            myadjmat[mynote][nextnot] += 1
            myadjmat[nextnot][mynote] += 1

    print(matdims)
    print("BREAK")
    return myadjmat


def gen_adjmat(music):
    matdims = max(set(music)) + 1
    myadjmat = np.zeros((matdims, matdims), dtype=int)
    for i in range(len(music) - 1):
        mynote = music[i]
        nextnot = music[i + 1]
        myadjmat[mynote][nextnot] = 1
        myadjmat[nextnot][mynote] = 1
    return myadjmat


def gen_diradjmat(music):
    matdims = max(set(music)) + 1
    myadjmat = np.zeros((matdims, matdims), dtype=int)
    for i in range(len(music) - 1):
        mynote = music[i]
        nextnot = music[i + 1]
        myadjmat[mynote][nextnot] = 1

    return myadjmat


def gen_degmat(music):
    matdims = max(set(music)) + 1
    myadjmat = np.zeros((matdims,matdims), dtype=int)
    for i in range(len(music) - 1):
        mynote = music[i]
        nextnot = music[i + 1]
        myadjmat[mynote][nextnot] = 1
        myadjmat[nextnot][mynote] = 1


    mydegmat = np.zeros((matdims, matdims), dtype=int)
    for i in range(len(myadjmat)):
        mydegmat[i][i] = sum(myadjmat[i])

    return mydegmat


def gen_laplace(music):
    degmat = gen_degmat(music)
    adjmat = gen_adjmat(music)
    lapmat = degmat - adjmat

    return lapmat



def delempties(arin):

    arin = np.delete(arin,np.where(~arin.any(axis=0))[0], axis=1)
    arin = np.delete(arin, np.where(~arin.any(axis=1))[0], axis=0)

    return arin


def drawgraph(music, mattype, drawtype):

    mymat = mattype(music)
    print("HI")
    mymat = delempties(mymat)

    maxval = 0
    for i in range(len(mymat)):
        for j in range(len(mymat)):
            if mymat[i][j] > maxval:
                maxval = mymat[i][j]
    print(maxval)

    G = nx.Graph()

    for i in range(len(mymat)):
        for j in range(len(mymat)):
            if mymat[i][j] > 0:
                G.add_edge(str(i), str(j), weight=mymat[i][j]/maxval)

    pos = drawtype(G)

    for i in range(10):
        edgeset = [(u, v) for (u, v, d) in G.edges(data=True) if (
                    float("0." + str(i)) < d['weight'] < float("0." + str(i + 1)))]
        nx.draw_networkx_edges(G, pos, edgelist=edgeset, width=math.sqrt(i + 1) )

    # nodes
    nx.draw_networkx_nodes(G, pos, node_size=20)

    # labels
    #nx.draw_networkx_labels(G, pos, font_size=20, font_family='sans-serif')

    # nx.draw_spring(G1, node_size=2)
    # nx.draw_circular(G1, node_size=2)
    #G1 = nx.convert_matrix.from_numpy_array(mymat)
    mpl.show()


def hyperplot(mattype, analtype):

    matlist = []
    for i in range(1, 7):
        mymusic = windowedmusic("music", i, True, analtype)
        matlist.append(delempties(mattype(mymusic)))
        mpl.plot(mymusic)
    mpl.show()

    eiglist = []
    for i in matlist:
        try:
            eig = np.sort(np.linalg.eigvals(i))[-2]
            eiglist.append(eig)
        except:
            pass

    mpl.plot(eiglist)
    print(eiglist)
    mpl.show()



analysistype = readmelody
mymusic = analysistype("melody.csv")
#mymusic = windowedmusic("music.mid", 2, True, analysistype)
drawgraph(mymusic, gen_weighted_adjmat, nx.spring_layout)
#djmat, nx.spring_layout)
